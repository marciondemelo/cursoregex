const texto = '.$+*?-'

//console.log(texto.match(/[]/g))
console.log(texto.match(/[+.?*$]/g)) // não precisa de escape dentro do conjunto (a maior parte dos caracteres são encarados como literais)
console.log(texto.match(/[$-?]/g)) // isso é um intervalo (range)

// NÂO é um intervalo (range)
console.log(texto.match(/[$\-?]/g))
console.log(texto.match(/[-?]/g))

// pode precisar de escape dentro do conjunto: - [ ] ^