const texto = 'ABC [abc] a-c 1234'

console.log(texto.match(/[a-c]/g))
console.log(texto.match(/a-c/g)) // não define um range

console.log(texto.match(/[A-z]/g)) // intervalos usam a ordem da tabela UNICODE (https://unicode-table.com/pt/)
/*
linhas de 0040 a 0070 da tabela unicode
@ABCDEFGHIJKLMNO
PQRSTUVWXYZ[\]^_
`abcdefghijklmno
pqrstuvwxyz{|}~␡
*/

// tem que respeitar a ordem da tabela UNICODE
// os dois exemplos estão fora de ordem. estaão de traz para frente, gerando uma excessão na execução
console.log(texto.match(/[a-Z]/g)) 
console.log(texto.match(/[4-1]/g))