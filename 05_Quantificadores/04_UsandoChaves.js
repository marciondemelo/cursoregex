const texto = 'O João recebeu 120 milhões apostando 6 9 21 23 45 46.'

// para definir ma quantidicador usa {}

//NUMEROS
console.log("todo conjunto de caracteres que inicie com digitos e que tenha pelo menos de 1 ou 2 digitos")
console.log(texto.match(/\d{1,2}/g)) 
console.log("todo conjunto de caracteres que inicie com digitos e que tenha pelo menos de 1 ou 2 digitos (utilizando texto alternativo 0123456789)")
console.log("0123456789".match(/\d{1,2}/g)) 
console.log("todo conjunto de caracteres que inicie com digitos e que tenha pelo menos de 2 digitos")
console.log(texto.match(/\d{2}/g))
console.log("todo conjunto de caracteres que inicie com digitos e que tenha pelo menos de 2 digitos (utilizando texto alternativo 0123456789)")
console.log("0123456789".match(/\d{2}/g))
console.log("todo conjunto de caracteres que inicie com digitos e que tenha o tamanho de 1 ou mais digitos")
console.log(texto.match(/\d{1,}/g))

//LETRAS
console.log("todo conjunto de caracteres que inicie com letras e que tenha pelo menos de 7 letras (não conta letas acentuadas)")
console.log(texto.match(/\w{7}/g))
console.log("todo conjunto de caracteres que inicie com letras e que tenha pelo menos de 7 letras (mesmo que um dos caracteres seja 'ã' ou 'õ')")
console.log(texto.match(/[\wõã]{7}/g))
console.log("todo conjunto de caracteres que inicie com letras e que tenha pelo menos de 7 letras (mesmo que um dos caracteres tenha qualquer tipo de acento)")
console.log(texto.match(/[\w[À-ü]{7}/g))
console.log("todo conjunto de caracteres que inicie com letras e que tenha pelo menos de 4 à 7 letras (mesmo que um dos caracteres tenha qualquer tipo de acento)")
console.log(texto.match(/[\w[À-ü]{4,7}/g))


console.log(" no futuro ....")
console.log("todo conjunto de caracteres que inicie com digitos e que tenha 1 ou 2 digitos. não retornara conunto com mais digitos")
console.log(texto.match(/\b\d{1,2}\b/g))
console.log("todo conjunto de caracteres que inicie com letras e que tenha pelo menos de 4 à 7 letras (mesmo que um dos caracteres tenha qualquer tipo de acento).não retornara conunto com mais digitos")
console.log(texto.match(/\b[\wÀ-ü]{4,7}\b/g))