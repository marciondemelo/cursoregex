const texto1 = 'De longe eu avistei o fogo e um pessoa gritando: FOGOOOOO'
const texto2 = 'There is a big fog in NYC'

// * -> zero ou mais
const regexGuloso = /fogo*/gi
const regexNaoGuloso = /fogo*?/gi
console.log(texto1.match(regexGuloso))
console.log(texto2.match(regexGuloso))

console.log("regexNaoGuloso")
console.log(texto1.match(regexNaoGuloso))
console.log(texto2.match(regexNaoGuloso))