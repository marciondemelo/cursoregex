let texto = 'Bom\ndia'
console.log(texto)
console.log(texto.match(/.../))
console.log(texto.match(/..../))
texto = 'Bom\tdia'
console.log(texto)
console.log(texto.match(/.../))
console.log(texto.match(/..../))

// o ponto identifica tabs (\t) mas não identifica enters (\n)